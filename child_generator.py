import sys

def generate_job(name):
    return f"""
image: python:latest

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"

cache:
  paths:
    - .cache/pip
    - venv/

before_script:
  - python --version ; pip --version  
  - pip install virtualenv
  - virtualenv venv
  - source venv/bin/activate  
test-{name}-job:
  script:
    - |
       cd "$CI_PROJECT_DIR/prod/{name}"
       python main.py
"""

def main(names):
    with open("child_pipeline.yml", "w") as f_out:
        for name in names:
            f_out.write(generate_job(name))


if __name__ == "__main__":
    names = sys.argv[1].split(",")
    main(names)